variable "access_key" {}

variable "secret_key" {}

variable "region" {}

variable "key_name" { default = "key1" }
	

resource "tls_private_key" "example" {
	  algorithm = "RSA"
	  rsa_bits  = 4096
}
	

resource "aws_key_pair" "generated_key" {
	  depends_on = [
	    tls_private_key.example
	  ]
	  key_name   = "${var.key_name}"
	  public_key = "${tls_private_key.example.public_key_openssh}"
}