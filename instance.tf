resource "aws_instance" "web-server" {
  ami           = "ami-0af2f764c580cc1f9"
  instance_type = "t2.micro"
  count         = "3"

  # Security group assign to instance
  vpc_security_group_ids = [aws_security_group.web-server.name]

  # key name
  key_name = aws_key_pair.generated_key.key_name

  user_data = <<EOF
		#! /bin/bash
        sudo yum update -y
        sudo yum install epel-release -y
		sudo yum install nginx -y 
		sudo systemctl enable nginx
		sudo systemctl start nginx
		echo "<h1>Deployed ELB Instance Example 2</h1>" | sudo tee /var/www/html/index.html
	EOF
  
  lifecycle {
    create_before_destroy = true
  }
  
  tags = {
    Name = "Toppan_instance-${count.index}"
  }
}