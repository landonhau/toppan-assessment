# create application load balancer
resource "aws_alb" "application_lb" {
  name               = "Toppan-alb"
  internal           = false
  load_balancer_type = "application"
  ip_address_type    = "ipv4"
  security_groups    = [aws_security_group.web-server.id]
  subnets            = data.aws_subnet_ids.subnet.ids

  tags   = {
    Name = "Toppan-alb"
  }
}

# create target group
resource "aws_lb_target_group" "target_group" {
  name        = "Toppan-tg"
  target_type = "instance"
  port        = 80
  protocol    = "HTTP"
  vpc_id      = data.aws_vpc.default.id

  health_check {
    enabled             = true
    interval            = 10
    path                = "/"
    protocol            = "HTTP"
    timeout             = 5
    healthy_threshold   = 5
    unhealthy_threshold = 2
  }
}

# create a listener 
resource "aws_lb_listener" "alb_listener" {
  load_balancer_arn = aws_alb.application_lb.arn
  port              = 80
  protocol          = "HTTP"
  default_action {
    target_group_arn    = aws_lb_target_group.target_group.arn
    type                = "forward"
  }
}

resource "aws_lb_target_group_attachment" "ec2_attach" {
    count               = length(aws_instance.web-server)
    target_group_arn    = aws_lb_target_group.target_group.arn
    target_id           = aws_instance.web-server[count.index].id

}