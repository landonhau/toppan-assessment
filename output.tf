output "elb-dns-name" {
  value = aws_alb.application_lb.dns_name
}
