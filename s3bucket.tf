resource "aws_s3_bucket" "b" {
	  bucket = "incremental-decremental-testing"
	  #region = "ap-southeast-1"
	  force_destroy = true
	  tags = {
	    Name        = "web_bucket"
	    Environment = "Testing"
	  } 
}
	

	

resource "aws_s3_bucket_object" "s3_object" {
	  depends_on = [
	    aws_s3_bucket.b,
	  ]
	  bucket = aws_s3_bucket.b.id
	  key    = "content"
	  acl    = "public-read"
	  source = "/Users/snsoft_om/Documents/Incre-decre-counter.zip"
}
